from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Hats, LocationVO
from common.json import ModelEncoder
import json

#Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href"
    ]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style",
        "location",
        'id',
    ]
    def get_extra_data(self, o):
        return {"location": o.location.id}

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style",
        "picture",
        "color",
        "fabric",
        "location"
    ]
    # encoders = {
    #     "location": LocationVOEncoder(),
    # }
    def get_extra_data(self, o):
        return{"location": o.location.id}




@require_http_methods(["GET", "POST"])
def api_hats_list(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder = HatsListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:

            location = LocationVO.objects.get(id=content["location"])
            content["location"] = location
            hat = Hats.objects.create(**content)

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )


        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET"])
def api_delete_hats(request, id):
    if request.method == "GET":
        hat = Hats.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False
        )

    try:
        hat = Hats.objects.get(id=id)
        hat.delete()
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    except Hats.DoesNotExist:
        return JsonResponse({"message": "Does not exist"})
