from django.db import models

# Create your models here.

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)


class Hats(models.Model):
    style = models.CharField(max_length = 100)
    picture = models.URLField(null=True)
    color = models.CharField (max_length = 100)
    fabric = models.CharField(max_length = 100)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )
