from django.urls import path
from .views import api_hats_list, api_delete_hats

urlpatterns = [
    path("", api_hats_list, name="api_hats_list"),
    path("<int:id>/",api_delete_hats, name="api_delete_hats")
]
