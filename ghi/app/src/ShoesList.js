import { useEffect, useState } from 'react'
import { Link } from "react-router-dom";

function ShoesList() {

  const [shoes, setShoes] = useState([])
  const [bins, setBins] = useState([])

  const fetchBins = async () => {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);

    if (response.ok) {
      const binData = await response.json();
      setBins(binData.bins);
      console.log("data***********************",binData.bins[0].closet_name)
      console.log("bins***********************",bins)
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8080/shoes/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setShoes(data.shoes)
    }
  }
  useEffect(()=> {
    fetchData();
    fetchBins();

  }, [])

  const handleDelete = async (e) => {
    console.log(e.target)
    const url = `http://localhost:8080/shoes/${e.target.id}`

    const fetchConfigs = {
        method: "Delete",
        headers: {
            "Content-Type": "application/json"
        }
    }
    const resp = await fetch(url, fetchConfigs)
    const data = await resp.json()

    setShoes(shoes.filter(shoe => String(shoe.id) !== e.target.id))

  }
  console.log("shoes****************", shoes)
  return (
    <>
    {shoes.map(shoe => {
      return (
        <div className="col-sm-3">
          <div className="card col shadow mb-2 bg-white rounded">
            <img src={shoe.picture} className=".img-fluid img-thumbnail"/>
            <div className="card-body">
              <h6 className="card-title">{shoe.manufacturer}</h6>
              <h4 className="card-title"><Link to={`/shoes/${shoe.id}`}>{shoe.model_name}</Link></h4>
            </div>
            <div className="card-footer">
            <button onClick={handleDelete} id={shoe.id} className="btn btn-danger">Delete</button>
            </div>
          </div>
        </div>
      );
    })}
    </>
  );
};
export default ShoesList;
