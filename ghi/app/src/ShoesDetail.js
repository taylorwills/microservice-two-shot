import { useParams, Link } from 'react-router-dom'
import { useState, useEffect} from 'react'

const ShoesDetail = () => {
    const [shoe, setShoe] = useState([])
    const { id } = useParams()

    const getData = async () => {
        const resp = await fetch(`http://localhost:8080/shoes/${id}`)
        if (resp.ok) {
            const data = await resp.json()
            setShoe(data)
            console.log(data)
        }
    }

    useEffect(()=> {
        getData()
    }, [])

    const handleDelete = async (e) => {
        const url = `http://localhost:8080/shoes/${id}`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const resp = await fetch(url, fetchConfigs)
        const data = await resp.json()
        setShoe(shoe.filter(shoe => String(shoe.id) !== e.target.id))
    }
    return <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <img src={shoe.picture} className=".img-fluid img-thumbnail"/>
                    <h1>Manufacturer:{shoe.manufacturer}</h1>
                    <h4>Model: {shoe.model_name}</h4>
                    <h4>Bin: {shoe.bin}</h4>
                    <h4></h4>

                    <Link to='/shoes' className="btn btn-primary">Return to Shoes List</Link>
                    <button onClick={handleDelete} className="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
    </>
}

export default ShoesDetail;
