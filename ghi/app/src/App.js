import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList'
import ShoesForm from './ShoesForm'
import ShoesDetail from './ShoesDetail'
import HatsList from './HatList';
import HatsForm from './HatForm';
import HatDetail from './HatDetail';

function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
        </Routes>
          <div className="row row-cols-6">
        <Routes>
          <Route path="/shoes" element={<ShoesList />} />
        </Routes>
        </div>
        <Routes>
          <Route path="/shoes/:id/" element={<ShoesDetail />} />
          <Route path="/shoes/new/" element={<ShoesForm />} />
          <Route path="hats">
            <Route path=""  element={<HatsList />} />
            <Route path="new" element={<HatsForm />} />
            <Route path=":id" element={<HatDetail />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
