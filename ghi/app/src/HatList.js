import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function HatsList() {
    const [hats, setHats] = useState([])

    const getData = async() => {
        const response = await fetch('http://localhost:8090/hats/');

        if(response.ok) {
            const data = await response.json();
            setHats(data.hats)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    const handleDelete = async (e) =>{
        const url = `http://localhost:8090/hats/${e.target.id}`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const resp = await fetch(url, fetchConfigs)
        const data = await resp.json()

        setHats(hats.filter(hat => String(hat.id) !== e.target.id))

    }
    return(
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Style</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat =>{
                    return(
                        <tr key={ hat.href }>
                            <td><Link to={`${hat.id}/`}>{hat.style}</Link></td>
                            <td> {hat.location} </td>
                            <td><button onClick={handleDelete} id={hat.id} className="btn btn-danger">Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default HatsList;
