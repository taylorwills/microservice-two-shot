# Wardrobify

Team:

* Taylor Wills - Shoes
* Ross Appelbaum - Hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
My hats model has all the required fields all of which work perfectly. It is able to communicate with the Wardrobe microservice through polling and the LocationVO which has the fields closet_name, section_number, shelf_number, and import_href.
