from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoes, BinVO


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "id",
    ]

class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "model_name",
        "manufacturer",
        "color",
        "picture",
        "bin",
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.id}

class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "model_name",
        "manufacturer",
        "color",
        "picture",
        "bin",
    ]
    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}





@require_http_methods(["GET", "POST"])
def shoes_list(request):

    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse({
            "shoes": shoes,
        }, encoder=ShoesListEncoder, safe=False)
    else:
        content = json.loads(request.body)
        print(content)

        try:
            bin = BinVO.objects.get(id=content["bin"])
            content["bin"] = bin
            shoes = Shoes.objects.create(**content)

        except BinVO.DoesNotExist:
            return JsonResponse({
                "message": "Invalid bin ID"
                },
                status=400,
            )
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def shoe_delete(request, id):

    if request.method == "GET":
        shoe = Shoes.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False
        )
    else:
        try:
            shoe = Shoes.objects.get(id=id)
            shoe.delete()

            return JsonResponse(
                shoe,
                encoder=ShoesDetailEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
