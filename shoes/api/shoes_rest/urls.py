from django.urls import path
from .views import shoes_list, shoe_delete

urlpatterns = [
    path('', shoes_list, name="shoes_list"),
    path("<int:id>/", shoe_delete, name="shoe_delete"),
    path("<int:id>/", shoe_delete, name="shoe_delete")
]
